var currentDate = new Date();
var calendarYear = currentDate.getFullYear();
var calendarMonth = currentDate.getMonth();
var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

function getCalendarFirstDate() {
	return new Date(calendarYear, calendarMonth, 1);
}

function getCalendarLastDate() {
	var dateMonth = calendarMonth + 1;
	return new Date(calendarYear, dateMonth, 0);
}

function createCalendarDayHTML(dayNumber) {
	return '<div class="calendar--day">' +
				'<div class="number">' + dayNumber + '</div>' +
				'<div class="items"></div>' +
			'</div>';
}

function nextMonth() {
	if (calendarMonth < 11) {
		calendarMonth++;
	} else {
		calendarYear++
		calendarMonth = 0;
	}
}

function prevMonth() {
	if (calendarMonth > 0) {
		calendarMonth--;
	} else {
		calendarYear--
		calendarMonth = 11;
	}
}

function updateCalendarTitle() {
	document
		.querySelector('.calendar--header--title')
		.innerHTML = months[calendarMonth] + ' ' + calendarYear;
}

function updateCalendarDays() {
	var firstDate = getCalendarFirstDate();
	var lastDate = getCalendarLastDate();
	var firstDayIndex = firstDate.getDay();
	var lastDayIndex = lastDate.getDay();
	var calendarBody = document.querySelector('.calendar--body');
	var oldCalendarDays = document.querySelectorAll('.calendar--day');

	firstDayIndex = firstDayIndex === 0 ? 6 : firstDayIndex - 1;
	lastDayIndex = lastDayIndex === 0 ? 6 : lastDayIndex - 1;

	for (var i = 0; i < oldCalendarDays.length; i++) {
		oldCalendarDays[i].remove();
	}

	for (var i = 0; i < firstDayIndex; i++) {
		calendarBody.innerHTML += createCalendarDayHTML('');
	}

	for (var i = 1; i <= lastDate.getDate(); i++) {
		calendarBody.innerHTML += createCalendarDayHTML(i);
	}

	for (var i = 0; i < 6 - lastDayIndex; i++) {
		calendarBody.innerHTML += createCalendarDayHTML('');
	}
}

function updateCalendar() {
	updateCalendarTitle();
	updateCalendarDays();
}

document
	.querySelector('.calendar--header--next')
	.addEventListener('click', function() {
		nextMonth();
		updateCalendar();
	});

document
	.querySelector('.calendar--header--prev')
	.addEventListener('click', function() {
		prevMonth();
		updateCalendar();
	});

updateCalendar();
