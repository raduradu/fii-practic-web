SETUP and Usage

1. Install node js https://nodejs.org/en/.
2. Open up a console (Windows Command Prompt, Git Bash, etc.).
3. Make sure you navigate in server folder.
4. Execute `npm install`.
5. Execute `node index.js`.
6. Check (directly in browser or via an Ajax/fetch call) the http://localhost:8765/events or http://127.0.0.1:8765/events.
7. Confirm there are no errors in the console, and that server responded with a json data.