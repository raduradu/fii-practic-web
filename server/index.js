const express = require('express');
const app = express();
const port = 8765;
const fs = require('fs');
const bodyParser = require('body-parser')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let DATA = {
	events: []
};

fs.readFile('./db/db.json', 'utf8', function(err, contents) {
    if (err) {
		console.log(err.message);
	} else {
		DATA = JSON.parse(contents);
	}
});

app.post('/event', (req, res) => {
	DATA.events.push(req.body);
	res.send("OK");
});

app.post('/events/save', (req, res) => {
	fs.writeFile("./db/db.json", JSON.stringify(DATA), function(err) {
		if(err) {
			res.send("ERROR");
		} else {
			res.send("OK");
		}
	}); 
	
});

app.get('/events', (req, res) => {
	res.send(JSON.stringify(DATA.events));
});

app.put('/event/:id', (req, res) => {
	DATA.events[req.params.id] = req.body;
	res.send("OK");
});

app.delete('/event/:id', (req, res) => {
	DATA.events[req.params.id] = null
	res.send("OK");
});

app.listen(port, () => console.log(`App listening on port ${port}!`));